> This project has been archived. It is no longer maintained or updated. If you have any questions about the project, please contact info@openlogisticsfoundation.org.

<!--
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3.
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3
-->

# Token Manager

**Token Manager** is a blockchain built using Cosmos SDK and Tendermint and created
with [ignite](https://github.com/ignite/cli).

## Cloning and updating of the repository

The Token Manager uses the modules
[TokenWallet](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/modules/modules/tokenwallet)
,
[Token](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/modules/modules/token),
[BusinessLogic](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/modules/modules/businesslogic)
and
[HashToken](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/modules/modules/hashtoken)
, which are included as submodules. To clone all repositories at once, you can use the command

```
git clone --recursive git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/tokenmanager.git
```

To update all repositories at one, you can use

```
git pull --recurse-submodules
```

To update the submodules use

```
git submodule update --init --recursive
```

## Documentation

The [documentation](https://git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/documentation/-/blob/main/index.adoc)
provides extensive information about the Token Manager and its components.

## Installation and Configuration

The installation and setup of the Token Manager is described
in [chapter 12](https://git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/documentation/-/blob/main/index.adoc)
of the documentation.

Your blockchain can be configured with `config.yml` during development. To learn more see
the [ignite documentation](https://github.com/ignite/cli#documentation).

## Launch

To launch your blockchain live on multiple nodes use `ignite network` commands. Learn more
about [Starport Network](https://github.com/tendermint/spn).

## CLI commands

The following commands are supported by the token manager module

### Transactions

All business logic module transaction commands need a preceding `TokenManagerd tx BusinessLogic `

* `create-token [token-type] [change-message] [segment-id] [module-ref]` to create a token and assign it to a segment
* `update-token [token-ref-id] [token-type] [change-message] [segment-id] [module-ref]`
* `activate-token [token-id] [segment-id] [module-ref]`  to activate a token and assign it to a segment
* `deactivate-token [tokenId]` to deactivate a token. This will also remove it from the containing segment
* `move-token-to-wallet [token-ref-id] [source-segment-id] [target-wallet-id]` to move a token to another wallet
* `clone-token [token-id] [wallet-id] [module-ref]`
* `create-hash-token [change-message] [segment-id] [document] [hash] [hash-function] [metadata]`
* `fetch-tokens-from-wallet [id]` to list all tokens from a specified wallet
* `fetch-tokens-from-segment [id]` to list all tokens from a specified segment
* `fetch-document-hash [id]` to fetch a document hash
* `revert-to-genesis` to revert the blockchain to its genesis state

All token wallet module transaction commands need a preceding `TokenManagerd tx Wallet`

* `fetch-segment [id]` to view details of a segment
* `fetch-all-segments` to list all segments
* `fetch-segment-history [id]` to view a specific segment's history
* `fetch-all-segment-histories`  to view all segments' histories
* `fetch-token-history-global [id]` to lookup all wallets a specific token has been contained by
* `fetch-all-tokens-history-global` to lookup all tokens and a list of wallets they have been contained by
* `fetch-token-wallet [id]` to view details of a wallet
* `fetch-all-token-wallets` to list all wallets
* `fetch-token-wallet-history [id]` to view the history of a specific wallet
* `fetch-all-token-wallet-histories` to view all wallets' histories
* `create-segment [name] [info] [wallet-id]` to create a new segment in a wallet
* `create-segment-with-id [id] [name] [info] [wallet-id]`
* `update-segment [id] [name] [info]` to change a segment's name and/or info
* `create-token-ref [id] [module-ref] [segment-id]` to create a new reference for a token in a segment
* `move-token-ref-to-segment [token-ref-id] [source-segment-id] [target-segment-id]` to move a token to another segment
* `create-wallet [name]` to create a new wallet
* `create-wallet-with-id [id] [name]`
* `update-wallet [id] [name]` to change a wallet's name
* `create-wallet-account [cosmos-address] [wallet-id]` to assign a cosmos-account to a wallet

All token module transaction commands need a preceding `TokenManagerd tx Token`

* `create-token [id] [token-type] [change-message] [segment-id]` to create a new token
* `update-token [id] [token-type] [change-message]` to change a token type, including a change message
* `update-token-information [token-id] [data]` to update a token's data
* `activate-token [id]` to set a token's valid flag to active
* `deactivate-token [id]` to set a token's valid flag to inactive
* `fetch-token [id]` to view a specific token
* `fetch-all-tokens`  to list all tokens
* `fetch-token-history [id]` to view a specific token's history
* `fetch-all-token-histories`  to view all token's histories

## Learn more

- [ignite](https://github.com/ignite/cli)
- [Cosmos SDK documentation](https://docs.cosmos.network)
- [Cosmos SDK Tutorials](https://tutorials.cosmos.network)
- [Discord](https://discord.gg/W8trcGV)
