// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package app

import (
	"encoding/json"
	"time"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/tendermint/starport/starport/pkg/cosmoscmd"
	tendermintAbciTypes "github.com/tendermint/tendermint/abci/types"
	"github.com/tendermint/tendermint/libs/log"
	protoTendermintTypes "github.com/tendermint/tendermint/proto/tendermint/types"
	tendermintTypes "github.com/tendermint/tendermint/types"
	tendermintTmDb "github.com/tendermint/tm-db"

	authorizationKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/keeper"
	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	hashtokenKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/keeper"
	hashtokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/types"
	tokenKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/keeper"
	tokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
	tokenwalletKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/keeper"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

const (
	CreatorA = "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"
	CreatorB = "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462x"

	TokenCreator = "Example Token Creator"
	TokenId1     = "T0"
	TokenId2     = "T1"
	TokenType    = "Example Token Type"

	WalletId1  = "W0"
	WalletId2  = "W1"
	WalletName = "Example Wallet Name"

	SegmentId1  = "S0"
	SegmentId2  = "S1"
	SegmentName = "Example Segment Name"
	SegmentInfo = "Example Segment Info"

	Description  = "description"
	Timestamp    = "2000-04-01T08:59:07+00:00"
	Changed      = "changed"
	NotChanged   = "not changed"
	Document     = "document"
	Hash         = "hash"
	HashFunction = "sha256"
	Metadata     = "metadata"

	SuccessfulTx    = "Successful Tx"
	UnsuccessfulTx  = "Unsuccessful Tx"
	SegmentNotFound = "Segment not found"
	WalletNotFound  = "Wallet not found"
	EmptyRequest    = "Empty Request"
	ValidRequest    = "Valid Request"
)

var defaultConsensusParams = &tendermintAbciTypes.ConsensusParams{
	Block: &tendermintAbciTypes.BlockParams{
		MaxBytes: 200000,
		MaxGas:   2000000,
	},
	Evidence: &protoTendermintTypes.EvidenceParams{
		MaxAgeNumBlocks: 302400,
		MaxAgeDuration:  504 * time.Hour,
	},
	Validator: &protoTendermintTypes.ValidatorParams{
		PubKeyTypes: []string{
			tendermintTypes.ABCIPubKeyTypeEd25519,
		},
	},
}

// CreateTestInput returns a testable app and the sdk context
func CreateTestInput() (*App, sdk.Context) {

	app := Setup(false)
	ctx := app.BaseApp.NewContext(false, protoTendermintTypes.Header{})

	appCodec := app.AppCodec()

	app.WalletKeeper = *tokenwalletKeeper.NewKeeper(
		appCodec,
		app.GetKey(tokenwalletTypes.StoreKey),
		app.GetMemKey(tokenwalletTypes.MemStoreKey),
	)

	app.TokenKeeper = *tokenKeeper.NewKeeper(
		appCodec,
		app.GetKey(tokenTypes.StoreKey),
		app.GetMemKey(tokenTypes.MemStoreKey),
	)

	app.HashtokenKeeper = *hashtokenKeeper.NewKeeper(
		appCodec,
		app.GetKey(hashtokenTypes.StoreKey),
		app.GetMemKey(hashtokenTypes.MemStoreKey),
	)

	app.AuthorizationKeeper = *authorizationKeeper.NewKeeper(
		appCodec,
		app.GetKey(authorizationTypes.StoreKey),
		app.GetMemKey(authorizationTypes.MemStoreKey),
	)

	return &app, ctx
}

// Setup returns an initialized app
func Setup(isCheckTx bool) App {
	db := tendermintTmDb.NewMemDB()
	app := NewApp(log.NewNopLogger(), db, nil, true, map[int64]bool{}, DefaultNodeHome, 5, cosmoscmd.MakeEncodingConfig(ModuleBasics), EmptyAppOptions{})
	if !isCheckTx {
		// init chain must be called to stop deliverState from being nil
		genesisState := NewDefaultGenesisState(app.appCodec)
		stateBytes, err := json.MarshalIndent(genesisState, "", " ")
		if err != nil {
			panic(err)
		}

		// Initialize the chain
		app.InitChain(
			tendermintAbciTypes.RequestInitChain{
				Validators:      []tendermintAbciTypes.ValidatorUpdate{},
				ConsensusParams: defaultConsensusParams,
				AppStateBytes:   stateBytes,
			},
		)

	}
	return *app
}

// EmptyAppOptions is a stub implementing AppOptions
type EmptyAppOptions struct{}

// Get implements AppOptions
func (ao EmptyAppOptions) Get(o string) interface{} {
	return nil
}
