#!/bin/bash

# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

# turn on bash's job control
set -m

mkdir -p $HOME/.TokenManager

# Copy only if directory is really empty
if [ -z "$(ls -A $HOME/.TokenManager)" ]; then
  cp -r /.TokenManagerGenesis/. $HOME/.TokenManager/
fi

# Start the primary process and put it in the background
TokenManagerd start --log_level error &
# Start the helper process
# the my_helper_process might need to know how to wait on the
# primary process to start before it does its work and returns
sleep 20s # Wait for tendermint demon start

# create new user
if TokenManagerd keys list | grep 'user'; then
  echo "User 'user' already exists"
else
  echo "Add user 'user'"
  printf '%s\n' "$USER_MNEMONIC" | TokenManagerd keys add user --recover || exit 1
  yes | TokenManagerd tx bank send $(TokenManagerd keys show alice -a) $(TokenManagerd keys show user -a) 1token
fi

# create module related application roles

# now we bring the primary process back into the foreground
# and leave it there
fg %1
